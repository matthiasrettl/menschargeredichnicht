#include <stdexcept>
#include <memory>
#include <array>

#include "GameContainer.h"
#include "Field.h"

#include "Player.h"
#include "PlayerHuman.h"
#include "PlayerComputer.h"

#include "Stone.h"
#include "GameState.h"

#include "Rules.h"


void assert(bool result, std::string errorMessage) {
	if (!result)
		throw std::exception(errorMessage.c_str());
}

std::shared_ptr<Game::GameContainer> setUpNewGame() {
	std::shared_ptr<Game::GameContainer> game =
		std::make_unique<Game::GameContainer>();

	for (wchar_t playerSymbol : {L'!', L'?', L'%', L'$'})
		game->addNewPlayer<Game::PlayerComputer>(playerSymbol);

	game->createStonesAndFieldsFor4Players();
	game->setGameStateToInitial();
	return game;
}

bool testRules() {
	auto game = setUpNewGame();
	Game::Rules& rules = game->getRules();
	Game::GameState gameState{ *game->getGameState() };

	Game::Player* player1 = game->findPlayer(0);
	Game::Field* startField1 = player1->getStones()[0]
		->getHomeField()->next(*player1);

	Game::Player* player2 = game->findPlayer(1);
	Game::Field* startField2 = player2->getStones()[0]
		->getHomeField()->next(*player2);

	Game::Player* player3 = game->findPlayer(2);
	Game::Field* startField3 = player3->getStones()[0]
		->getHomeField()->next(*player3);

	Game::Player* player4 = game->findPlayer(3);
	Game::Field* startField4 = player4->getStones()[0]
		->getHomeField()->next(*player4);

	std::array<Game::Field*, 40> trackFields{};
	Game::Field* tempField = startField1;
	for (int i = 0; i < 40; i++) {
		trackFields[i] = tempField;
		tempField = tempField->next(*player1);
	}

	//player 1 go out
	auto moves = rules.possibleMoves(*player1, 6, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	assert(moves.size() == 1, "player 1 go out size()");
	Game::Stone* stone1P1 = rules.getStoneOnField(startField1, gameState);
	assert(stone1P1, "player 1 go out startField1");

	//player 1 move
	moves = rules.possibleMoves(*player1, 6, gameState);
	gameState = moves[0].first;
	std::cout << moves[0].second << std::endl;
	assert(moves.size() == 1, "player 1 move 6 size");
	assert(gameState.state_[stone1P1] == trackFields[6], 
		"player 1 move 6 field 6");
	assert(!rules.getStoneOnField(startField1, gameState),
		"player 1 move 6 startField1");

	//player 1 go out 2
	moves = rules.possibleMoves(*player1, 6, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	assert(moves.size() == 1, "player 1 go out 2 size()");
	Game::Stone* stone2P1 = rules.getStoneOnField(startField1, gameState);
	assert(stone2P1, "player 1 go out 2 startField1");
	
	//player 1 move 2
	moves = rules.possibleMoves(*player1, 6, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	assert(moves.size() == 1, "player 1 move2 6 size");
	assert(gameState.state_[stone1P1] == trackFields[12], 
		"player 1 move2 6 trackFields[12]");
	assert(gameState.state_[stone2P1] == trackFields[0], 
		"player 1 move2 6 trackFields[0]");

	//player 1 move 3
	moves = rules.possibleMoves(*player1, 1, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	assert(moves.size() == 1, "player 1 move3 6 size");
	assert(gameState.state_[stone1P1] == trackFields[12],
		"player 1 move3 6 trackFields[12]");
	assert(gameState.state_[stone2P1] == trackFields[1],
		"player 1 move3 6 trackFields[1]");

	//player 2 go out 1
	moves = rules.possibleMoves(*player2, 6, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	assert(moves.size() == 1, "player 2 go out size");
	Game::Stone* stone1P2 = rules.getStoneOnField(startField2, gameState);

	//player 2 move 2 and throw stone 1 of player 1
	moves = rules.possibleMoves(*player2, 2, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	assert(moves.size() == 1, "player 2 go out size");
	assert(gameState.state_[stone1P2] == trackFields[12],
		"player 2 move 2 trackFields[12]");
	assert(gameState.state_[stone1P1] == stone1P1->getHomeField(),
		"player 2 move 2 home field");
	
	//player 2 go out with all stones
	moves = rules.possibleMoves(*player2, 6, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	Game::Stone* stone2P2 = rules.getStoneOnField(startField2, gameState);
	moves = rules.possibleMoves(*player2, 1, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;

	moves = rules.possibleMoves(*player2, 6, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	Game::Stone* stone3P2 = rules.getStoneOnField(startField2, gameState);
	moves = rules.possibleMoves(*player2, 3, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;

	moves = rules.possibleMoves(*player2, 6, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	Game::Stone* stone4P2 = rules.getStoneOnField(startField2, gameState);
	moves = rules.possibleMoves(*player2, 4, gameState);
	assert(moves.size() > 1, "player 2 go out2 size()");
	std::cout << moves[0].second << std::endl;
	gameState = moves.back().first;

	assert(gameState.state_[stone2P2] == trackFields[11],
		"player 2 go out2 trackfield[11]");
	assert(gameState.state_[stone3P2] == trackFields[13],
		"player 2 go out2 trackfield[13]");
	assert(gameState.state_[stone4P2] == trackFields[14],
		"player 2 go out2 trackfield[14]");

	// player 3 no move possible
	moves = rules.possibleMoves(*player3, 5, gameState);
	assert(moves.size() == 0, "player 3 no possible move size");
	
	// player 1 move to goal
	gameState.state_[stone2P1] = trackFields[39];
	moves = rules.possibleMoves(*player1, 5, gameState);
	assert(moves.size() == 0, "player 1 no possible move size");

	moves = rules.possibleMoves(*player1, 4, gameState);
	std::cout << moves[0].second << std::endl;
	gameState = moves[0].first;
	assert(moves.size() == 1, "player 1 move to goal size");
	assert(gameState.state_[stone2P1] == player1->getGoalFields().back(),
		"player 1 move to goal stone2P1");
	return true;
}

bool testWinner() {
	auto game = setUpNewGame();
	Game::Rules& rules = game->getRules();
	Game::GameState gameState{ *game->getGameState() };
	assert(!game->winner(gameState), "no winner");

	Game::Player* player1 = game->findPlayer(0);
	auto& stones = player1->getStones();
	auto& goalFields = player1->getGoalFields();

	gameState.state_[stones[0]] = goalFields[3];
	assert(!game->winner(gameState), "no winner");

	gameState.state_[stones[1]] = goalFields[2];
	gameState.state_[stones[2]] = goalFields[1];
	gameState.state_[stones[3]] = goalFields[0];
	assert(game->winner(gameState) == player1, "player 1 is the winner");
	return true;
}


bool testComputerPlay() {
	auto game = setUpNewGame();
	Game::Rules& rules = game->getRules();
	Game::GameState gameState{ *game->getGameState() };

	Game::Player* player = *game->getPlayers()->begin();
	while (!game->winner(*game->getGameState()))
	{
		size_t dice = player->dice();
		game->setGameState(player->move(dice, *game->getGameState(), rules));
		player = game->nextPlayer(player, dice);
	}

	Game::Player* winner = game->winner(*game->getGameState());
	std::cout << "winner is player " << winner->getPlayerId() << std::endl;
	
	return true;
}

int main() {
	try { testRules(); }
	catch (std::exception e) {
		std::cout << "test rules failed" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}

	try { testWinner(); }
	catch (std::exception e) {
		std::cout << "test winner failed" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}

	try { testComputerPlay(); }
	catch (std::exception e) {
		std::cout << "test computer failed" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}

	std::cout << "okay" << std::endl;
}