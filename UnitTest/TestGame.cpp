#include <stdexcept>
#include <memory>

#include "GameContainer.h"
#include "Field.h"

#include "Player.h"
#include "PlayerHuman.h"
#include "PlayerComputer.h"

#include "Stone.h"
#include "GameState.h"


void assert(bool result, std::string errorMessage) {
	if (!result)
		throw std::exception(errorMessage.c_str());
}

std::shared_ptr<Game::GameContainer> setUpNewGameWithPlayers() {
	std::shared_ptr<Game::GameContainer> game =
		std::make_unique<Game::GameContainer>();

	for (wchar_t playerSymbol : {L'!', L'?', L'%', L'$'})
		game->addNewPlayer<Game::PlayerComputer>(playerSymbol);
	
	return game;
}

std::shared_ptr<Game::GameContainer> setUpNewGame() {
	std::shared_ptr<Game::GameContainer> game = setUpNewGameWithPlayers();
	game->createStonesAndFieldsFor4Players();

	return game;
}

bool testAddNewPlayer() {
	Game::GameContainer game{};

	Game::PlayerComputer* player1 = 
		game.addNewPlayer<Game::PlayerComputer>(L'L');
	assert(game.getPlayers()->find(player1) != game.getPlayers()->end(),
		"add new player1");
	assert(game.findPlayer(player1->getPlayerId()) == player1,
		"find by playerid");
	assert(player1->getPlayerSymbol() == L'L',
		"player symbol of player 1");

	Game::PlayerHuman* player2 =
		game.addNewPlayer<Game::PlayerHuman>(L'H');
	assert(game.getPlayers()->find(player2) != game.getPlayers()->end(),
		"add new player2");
	assert(player2->getPlayerSymbol() == L'H',
		"player symbol of player 2");

	size_t maxDice = player1->dice();
	size_t minDice = maxDice;
	for (int i = 0; i < 1000 * 1000; ++i) {
		size_t dice = player1->dice();
		if (dice > maxDice)
			maxDice = dice;
		else if (dice < minDice)
			minDice = dice;
	}
	assert(maxDice == 6, "dice without 6");
	assert(minDice == 1, "dice without 1");

	return true;
}

bool testAddNewStone() {
	std::shared_ptr<Game::GameContainer> game = setUpNewGameWithPlayers();
	Game::Player* player1 = *game->getPlayers()->begin();
	Game::Stone* stone1 = game->addNewStone(player1);
	assert(stone1->getPlayer() == player1, "wrong player");
	assert(stone1 == player1->getStones().back(), "stone isn't assigned to player");

	std::shared_ptr<Game::GameContainer> game2 = setUpNewGameWithPlayers();
	try {
		Game::Stone* stone1 = game2->addNewStone(player1);
		assert(false, "player of wrong game");
	}
	catch (std::exception e) {}

	assert(game->getStones()->at(0) == stone1, "stone 1 not saved");
	assert(game->getStones()->size() == 1, "wrong number of stones");

	return true;
}

bool testAddNewField() {
	std::shared_ptr<Game::GameContainer> game = setUpNewGameWithPlayers();
	Game::Field* field1 = game->addNewField();

	assert(game->getFields()->at(0) == field1, "field 1 not saved");
	assert(game->getFields()->size() == 1, "wrong number of fields");

	return true;
}

bool testCreateFields() {
	std::shared_ptr<Game::GameContainer> game = setUpNewGameWithPlayers();
	
	game->createStonesAndFieldsFor4Players();
	assert(game->getFields()->size() == 72, "wrong number of fields");
	assert(game->getStones()->size() == 16, "wrong number of stones");
	assert((*game->getPlayers()->begin())->getGoalFields().size() == 4, "wrong number of assigned goal fields");
	assert((*game->getPlayers()->begin())->getStones().size() == 4, "wrong number of assigned stones");

	return true;
}

bool testGameState() {
	std::shared_ptr<Game::GameContainer> game = setUpNewGame();
	const Game::GameState* state = game->setGameStateToInitial();
	Game::GameState assignedState = *state;
	Game::GameState movedState = std::move(*state);
	Game::GameState copiedState = Game::GameState{ *state };

	auto pair = *state->state_.cbegin();
	assert(assignedState.state_[pair.first] == pair.second, "assigned state mutated");
	assert(movedState.state_[pair.first] == pair.second, "moved state mutated");
	assert(copiedState.state_[pair.first] == pair.second, "copied state mutated");

	assert(state->state_.size() == 16, "wrong number of state variables");
	assert(assignedState.state_.size() == 16, "wrong number of state variables in assigned state");
	assert(movedState.state_.size() == 16, "wrong number of state variables in moved state");
	assert(copiedState.state_.size() == 16, "wrong number of state variables in copied state");

	copiedState.state_.erase(copiedState.state_.begin(), ++copiedState.state_.begin());
	assert(state->state_.size() == 16, "wrong number of state variables after copied state changed");
	assert(assignedState.state_.size() == 16, "wrong number of state variables in assigned state after copied state changed");
	assert(movedState.state_.size() == 16, "wrong number of state variables in moved state after copied state changed");
	assert(copiedState.state_.size() == 15, "wrong number of state variables in copied state after copied state changed");

	movedState.state_.erase(movedState.state_.begin(), ++movedState.state_.begin());
	assert(state->state_.size() == 16, "wrong number of state variables after moved state changed");
	assert(assignedState.state_.size() == 16, "wrong number of state variables in assigned state after moved state changed");
	assert(movedState.state_.size() == 15, "wrong number of state variables in moved state after moved state changed");
	assert(copiedState.state_.size() == 15, "wrong number of state variables in copied state after moved state changed");

	assignedState.state_.erase(assignedState.state_.begin(), ++assignedState.state_.begin());
	assert(state->state_.size() == 16, "wrong number of state variables after assigned state changed");
	assert(assignedState.state_.size() == 15, "wrong number of state variables in assigned state after assigned state changed");
	assert(movedState.state_.size() == 15, "wrong number of state variables in moved state after assigned state changed");
	assert(copiedState.state_.size() == 15, "wrong number of state variables in copied state after assigned state changed");
	return true;
}

int main() {
	try { testAddNewPlayer(); } 
	catch (std::exception e) { 
		std::cout << "add new player failed" << std::endl; 
		std::cout << e.what() << std::endl;
		return -1;
	}

	try { testAddNewStone(); }
	catch (std::exception e) { 
		std::cout << "add new stone failed" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}

	try { testAddNewField(); }
	catch (std::exception e) { 
		std::cout << "add new field failed" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}

	try { testCreateFields(); }
	catch (std::exception e) { 
		std::cout << "create fields failed" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}

	try { testGameState(); }
	catch (std::exception e) {
		std::cout << "game state" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}

	std::cout << "okay" << std::endl;
}