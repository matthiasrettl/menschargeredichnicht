﻿# CMakeList.txt: CMake-Projektdatei der obersten Ebene. Führen Sie hier die globale Konfiguration aus,
# und schließen Sie Unterprojekte ein.
#
cmake_minimum_required (VERSION 3.8)

project ("MenschArgereDichNicht" CXX)
set (CMAKE_CXX_STANDARD 11)

enable_testing()

# Schließen Sie Unterprojekte ein.
add_subdirectory ("Game")
add_subdirectory ("UserInterface")
add_subdirectory ("UnitTest")
