#include "Stone.h"
#include "Player.h"

using namespace Game;


Stone::Stone(Player* player):
	player_{ player }
{
	if (player)
		player->getStones().push_back(this);
}


const Player * Stone::getPlayer() const {
	return player_;
}

void Stone::setHomeField(Field * homeField) {
	homeField_ = homeField;
}

Field * Stone::getHomeField() const {
	return homeField_;
}
