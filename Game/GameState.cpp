#include "GameState.h"

Game::GameState::GameState():
	state_{ std::unordered_map<Stone*, Field*>{} }
{}

Game::GameState::~GameState() {}

Game::GameState::GameState(const GameState & state) :
	state_{state.state_}
{}

Game::GameState::GameState(GameState && state) :
	state_{std::move(state.state_)}
{}

Game::GameState & Game::GameState::operator=(const GameState & state)
{
	this->state_ = state.state_;
	return *this;
}
