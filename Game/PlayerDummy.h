#pragma once

#include "Player.h"
#include "GameState.h"

namespace Game {

	class GameContainer;
	class Rules;

	class PlayerDummy : public Player
	{
	public:
		virtual ~PlayerDummy() {};

		virtual size_t dice() override {
			return 0;
		};

		virtual GameState move(size_t dice, GameState& currentState, Rules& rules) override {
			return currentState;
		};

		friend class GameContainer;
	private:
		PlayerDummy(size_t playerId, wchar_t playerSymbol);

		PlayerDummy(const PlayerDummy&);
		PlayerDummy& operator=(const PlayerDummy&);
	};
}