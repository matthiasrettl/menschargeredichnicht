#include "Player.h"

using namespace Game;

Player::Player(size_t playerId, wchar_t playerSymbol) :
	playerId_{ playerId },
	playerSymbol_{ playerSymbol }
{}

Player::~Player() {}

size_t Player::dice() {
	return 1 + (std::rand() % 6);
}

size_t Player::getPlayerId() const {
	return playerId_;
}

wchar_t Player::getPlayerSymbol() const {
	return playerSymbol_;
}

std::vector<Stone*>& Player::getStones() {
	return stones_;
}

std::vector<Field*>& Player::getGoalFields() {
	return goalFields_;
}

bool Game::Player::operator==(const Player & obj) const
{
	return obj.getPlayerId() == this->getPlayerId();
}

