#include "GameContainer.h"

using namespace Game;

GameContainer::GameContainer():
	rules_{ *this },
	currentGameState_{},
	players_{},
	stones_{},
	fields_{}
{
}

GameContainer::~GameContainer() {
	for (Player* player : players_)
		delete player;

	for (Stone* stone : stones_)
		delete stone;

	for (Field* field : fields_)
		delete field;
}

const std::unordered_set<Player*>* GameContainer::getPlayers() const {
	return &players_;
}

Player * GameContainer::findPlayer(size_t playerId) const {
	return *std::find_if(players_.begin(), players_.end(), [playerId](auto iter) -> bool {
		return playerId == iter->getPlayerId(); });
}

Stone * GameContainer::addNewStone(Player * player) {
	if (players_.end() == std::find(
		players_.begin(), players_.end(), player))
		throw std::invalid_argument("player must be in this gameContainer");

	Stone* newStone = new Stone{ player };
	stones_.push_back(newStone);

	return newStone;
}

const std::vector<Stone*>* GameContainer::getStones() const {
	return &stones_;
}

Field * GameContainer::addNewField() {
	fields_.push_back(new Field{});

	return fields_.back();
}

const std::vector<Field*>* GameContainer::getFields() const {
	return &fields_;
}

void GameContainer::createStonesAndFieldsFor4Players() {
	if (players_.size() != 4)
		throw std::exception("there must be exactly 4 players");

	//create stones
	for (auto player : *getPlayers())
		for (int i = 0; i < 4; i++)
			addNewStone(player);

	//create and assign fields
	createFields<40>(nullptr, 0);
}

GameState * GameContainer::setGameStateToInitial() {
	currentGameState_ = GameState::createInitialState(
		stones_.cbegin(), stones_.cend());

	return &currentGameState_;
}

GameState * GameContainer::getGameState() {
	return &currentGameState_;
}

void GameContainer::setGameState(GameState state) {
	currentGameState_ = state;
}
