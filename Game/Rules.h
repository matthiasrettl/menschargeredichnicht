#pragma once

#include <vector>
#include <stdexcept>
#include <sstream>

#include "Player.h"
#include "Stone.h"
#include "Field.h"
#include "GameState.h"

namespace Game {
	class GameContainer;

	using Move = std::pair<GameState, std::string>;

	class Rules
	{
	public:
		Rules(GameContainer& gameContainer);
		~Rules();

		Stone* isAPlayerStoneOnHisHomeField(Player& player,
			size_t dice, GameState& state) {

			auto stateMap = state.state_;
			for (Stone* playerStone : player.getStones())
				if (stateMap[playerStone] == playerStone->getHomeField())
					return playerStone;

			return nullptr;
		}

		Stone* isAPlayerStoneAtHisStartPosition(Player& player,
			size_t dice, GameState& state) {

			auto stateMap = state.state_;
			Field* startPosition = player.getStones()[0]->getHomeField()->next(player);
			for (Stone* playerStone : player.getStones())
				if (stateMap[playerStone] == startPosition)
					return playerStone;

			return nullptr;
		}

		Field* findNextField(Player& player, Stone* stone,
			size_t dice, GameState& state, size_t& fields_moved) {

			Field* currentField = state.state_[stone];
			Field* nextField = currentField;
			int points = static_cast<int>(dice);
			while (points > 0)
			{
				points -= static_cast<int>(nextField->cost(player));
				nextField = nextField->next(player);
				if (!nextField) return nullptr; //end reached
				fields_moved++;
			}

			if (points < 0) return nullptr;
			else return nextField;
		}

		Stone* getStoneOnField(Field* field, GameState& state) {
			for (auto entry : state.state_)
				if (entry.second == field) 
					return entry.first;

			return nullptr;
		}

		Move move(Player& player, Stone* stone,
			size_t dice, GameState state) {
			
			//create move description
			std::stringstream description{std::ios_base::out};

			//find stoneId
			size_t stoneId = 0;
			for (Stone* playerStone : player.getStones())
				if (playerStone == stone) break;
				else stoneId++;

			//find next field
			Field* currentField = state.state_[stone];
			Field* nextField = currentField;
			size_t fields_moved = 0;

			nextField = findNextField(player, stone, dice, state, fields_moved);
			if (!nextField) throw std::exception("beyond the board end");

			description << "player " << player.getPlayerId()+1
				<< " move stone " << stoneId << " "
				<< fields_moved << " fields";

			//check if next field is occupied
			Stone* poorStone = getStoneOnField(nextField, state);
			if (poorStone) {
				if (poorStone->getPlayer() == &player)
					//must not throw an own stone
					throw std::exception("suicide");

				//throw the stone on this field back home
				state.state_[poorStone] = poorStone->getHomeField();
				description << " and throw a stone of player "
					<< poorStone->getPlayer()->getPlayerId()+1;
			}

			// create the move
			state.state_[stone] = nextField;
			return Move{ state, description.str() };
		}

		std::vector<Move> possibleMoves(Player& player,
			size_t dice, GameState& state) {

			Stone* playerStoneAtHome = isAPlayerStoneOnHisHomeField(
				player, dice, state);

			Stone* playerStoneAtStart = isAPlayerStoneAtHisStartPosition(
				player, dice, state);


			if (playerStoneAtStart && playerStoneAtHome) {
				/*
				RULE: as long as a stone is at home, a player stone 
					on the start field must move as soon as possible
				*/
				try {
					return { move(player, playerStoneAtStart, dice, state) };
				}
				catch (std::exception e) {
					//tried to throw a stone of the same player
				}
			}

			if (dice == 6 && playerStoneAtHome && !playerStoneAtStart) {
				/*
				RULE: a 6 must be used to move a stone out of his home,
					except there is a own stone on the start field
				*/
				return { move(player, playerStoneAtHome, dice, state) };
			}
			
			//player can decide which stone he wants to move
			std::vector<Move> moves{};
			for (Stone* stone : player.getStones()) {
				if (state.state_[stone] == stone->getHomeField())
					continue; //cannot move out of his home
				
				try {
					moves.push_back(move(player, stone, dice, state));
				}
				catch (std::exception e) {
					//tried to throw a stone of the same player
					// or reached the end of the board
				}
			}
			return moves;
		}

	private:
		Rules(const Rules&);
		Rules& operator=(const Rules&);

		GameContainer& gameContainer_;
	};
}