#pragma once

#include <unordered_map>
#include<iostream>

namespace Game {

	class Field;
	class Stone;

	struct GameState
	{
		GameState();
		~GameState();

		GameState(const GameState& state);
		GameState(GameState&& state);
		GameState& operator=(const GameState& state);

		template <class StonePtrIterator>
		static GameState createInitialState(StonePtrIterator begin, StonePtrIterator end) {
			GameState newState = GameState{};
			std::unordered_map<Stone*, Field*>& state = newState.state_;
			for (auto iter = begin; iter != end; ++iter) {
				Stone* stone = dynamic_cast<Stone*>(*iter);
				Field* initialField = stone->getHomeField();

				state[stone] = initialField;
			}
			
			return newState;
		}

		std::unordered_map<Stone*, Field*> state_;
	};
}