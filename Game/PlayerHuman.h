#pragma once

#include <functional>
#include <memory>

#include "Player.h"
#include "GameState.h"
#include "Rules.h"

namespace Game {

	class GameContainer;

	class PlayerHuman : public Player
	{
	public:
		using UserChoiceFunction = std::function<size_t(std::vector<Move>&)>;

		virtual ~PlayerHuman() {};

		virtual GameState move(size_t dice, GameState& currentState, Rules& rules) override {
			auto moves = rules.possibleMoves(*this, dice, currentState);
			if (moves.empty()) return currentState;

			size_t moveId = (*userChoice_)(moves);
			GameState nextState = moves[moveId].first;
			std::cout << moves[moveId].second << std::endl;
			return nextState;
		};

		void setUserChoiceFunction(UserChoiceFunction userChoice) {
			userChoice_ = std::make_unique<UserChoiceFunction>(userChoice);
		};

		friend class GameContainer;
	private:
		PlayerHuman(size_t playerId, wchar_t playerSymbol);

		PlayerHuman(const PlayerHuman&);
		PlayerHuman& operator=(const PlayerHuman&);

		std::unique_ptr<UserChoiceFunction> userChoice_;
	};
}