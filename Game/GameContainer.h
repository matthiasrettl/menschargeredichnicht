#pragma once

#include <memory>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <type_traits>
#include <stdexcept>
#include <string>

#include "Field.h"
#include "Player.h"
#include "Stone.h"
#include "GameState.h"
#include "Rules.h"

namespace Game {

	class GameContainer
	{
	public:
		GameContainer();
		~GameContainer();

		Rules& getRules() {
			return rules_;
		}

		template<class PlayerType>
		PlayerType * addNewPlayer(wchar_t playerSymbol) {
			static_assert(
				std::is_base_of<Player, PlayerType>::value,
				"PlayerType must inherit from Player!");

			PlayerType* newPlayer = new PlayerType{ players_.size(), playerSymbol };
			Player* newBasePlayer = static_cast<Player*>(newPlayer);
			players_.insert(newBasePlayer);

			return newPlayer;
		}
		const std::unordered_set<Player*>* getPlayers() const;
		Player* findPlayer(size_t playerId) const;

		Stone* addNewStone(Player* player);
		const std::vector<Stone*>* getStones() const;

		Field* addNewField();
		const std::vector<Field*>* getFields() const;

		void createStonesAndFieldsFor4Players();

		GameState* setGameStateToInitial();
		GameState* getGameState();
		void setGameState(GameState state);

		Player* winner(const GameState& currentState) const {
			auto& state = currentState.state_;
			for (auto iter = players_.cbegin(); iter != players_.cend(); ++iter) {
				Player* player = *iter;
				auto& goalFields = player->getGoalFields();
				size_t stonesInGoal = 0;

				for (Stone* stone : player->getStones())
					if (goalFields.cend() != std::find(goalFields.cbegin(), 
						goalFields.cend(), state.at(stone)))
						stonesInGoal++;

				if (stonesInGoal == 4) return player;
			}

			return nullptr;
		}

		Player* nextPlayer(Player* currentPlayer, size_t dice) {

			if (dice == 6) return currentPlayer;
			size_t playerId = currentPlayer->getPlayerId();
			size_t nextPlayerId = (1 + playerId) % 4;
			return findPlayer(nextPlayerId);
		}


	private:
		GameContainer(const GameContainer&);
		GameContainer& operator=(const GameContainer&);

		template<size_t N>
		Field* createFields(Field* firstField, int playerId) {
			Field* newField = addNewField();

			int modulo10 = N % 10;
			if (modulo10 > 0 && modulo10 <= 2 && playerId > -1) {
				Player* player = findPlayer(playerId);
				Field* sideField = createFields<4>(newField, -1);

				if (modulo10 == 1) {
					// add home fields

					for (auto stoneIterator = player->getStones().begin();
						sideField != newField;
						stoneIterator++)
					{
						Field*const nextSideField = sideField->next(*player);
						sideField->setCosts(6);
						sideField->setNextDefaultField(newField);
						(*stoneIterator)->setHomeField(sideField); // assign this home field to a stone

						sideField = nextSideField;
					}

					// next player
					playerId = playerId + 1;
				}
				else {
					// add goal fields

					//player can go in his goal fields, but the other players should go on
					//newField->getNextFields().insert(player, sideField);
					newField->getNextFields()[player] = sideField;

					//go to the end of the goal fields and set the next default field NULL
					for (Field* nextSideField = sideField->next(*player);
						nextSideField != newField;
						nextSideField = sideField->next(*player)) {
						player->getGoalFields().push_back(sideField); // assign goalfield to player

						sideField = nextSideField;
					}

					sideField->setNextDefaultField(nullptr); //end reached
					player->getGoalFields().push_back(sideField); // assign goalfield to player
				}
			}

			newField->setNextDefaultField(createFields<N - 1>(
				firstField == nullptr ? newField : firstField, playerId));

			return newField;
		}

		template<>
		Field* createFields<0>(Field* firstField, int playerId) {
			return firstField;
		}

		Rules rules_;
		GameState currentGameState_;

		std::unordered_set<Player*> players_;
		std::vector<Stone*> stones_;
		std::vector<Field*> fields_;
	};
}