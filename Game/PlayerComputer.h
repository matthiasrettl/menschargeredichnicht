#pragma once

#include <iostream>

#include "Player.h"
#include "GameState.h"
#include "Rules.h"

namespace Game {

	class GameContainer;

	class PlayerComputer : public Player
	{
	public:
		virtual ~PlayerComputer() {};

		virtual GameState move(size_t dice, GameState& currentState, Rules& rules) override {
			auto moves = rules.possibleMoves(*this, dice, currentState);
			if (moves.empty()) return currentState;

			size_t moveId = std::rand() % moves.size();
			GameState nextState = moves[moveId].first;


			for (auto move : moves) {
				if (move.second.find("throw") != std::string::npos
					&& 80 > (std::rand() % 100)) {
					nextState = move.first;
				}
			}

			std::cout << moves[moveId].second << std::endl;
			return nextState;
		};

		friend class GameContainer;
	private:
		PlayerComputer(size_t playerId, wchar_t playerSymbol);

		PlayerComputer(const PlayerComputer&);
		PlayerComputer& operator=(const PlayerComputer&);
	};
}