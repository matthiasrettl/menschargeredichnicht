#pragma once

#include <iostream>
#include <vector>

namespace Game {

	class Field;
	class Stone;
	class GameState;
	class Rules;

	class Player
	{
	public:
		Player(size_t playerId, wchar_t playerSymbol);
		virtual ~Player();

		virtual size_t dice();
		virtual GameState move(size_t dice, GameState& currentState, Rules& rules) = 0;

		size_t getPlayerId() const;
		wchar_t getPlayerSymbol() const;

		std::vector<Stone*>& getStones();
		std::vector<Field*>& getGoalFields();

		bool operator ==(const Player& obj) const;
	private:
		Player(const Player&);
		Player& operator=(const Player&);

		const size_t playerId_;
		const wchar_t playerSymbol_;

		std::vector<Stone*> stones_;
		std::vector<Field*> goalFields_;
	};
}

namespace std {
	template<>
	struct hash<Game::Player>
	{
		size_t operator()(const Game::Player& obj) const {
			return obj.getPlayerId();
		}
	};
}