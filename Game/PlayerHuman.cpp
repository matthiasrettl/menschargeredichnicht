#include "PlayerHuman.h"

using namespace Game;

PlayerHuman::PlayerHuman(size_t playerId, wchar_t playerSymbol) :
	Player(playerId, playerSymbol),
	userChoice_{ nullptr }
{}
