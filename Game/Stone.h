#pragma once

namespace Game {

	class GameContainer;
	class Player;
	class Field;

	class Stone
	{
	public:
		~Stone() {};

		const Player* getPlayer() const;

		void setHomeField(Field* homeField);
		Field* getHomeField() const;

		friend class GameContainer;
	private:
		Stone(Player* player);

		Stone(const Stone&);
		Stone& operator=(const Stone&);

		//NO OWNERSHIP
		const Player* player_;
		Field* homeField_;
	};
}