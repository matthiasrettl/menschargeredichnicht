#pragma once

#include <iostream>
#include <map>

namespace Game {

	class GameContainer;
	class Player;

	class Field
	{
	public:
		~Field() {};

		Field *const next(Player& player) const {
			if (nextField_.find(&player) != nextField_.end())
				return nextField_.at(&player);
			else
				return nextDefaultField_;
		}

		size_t cost(Player& player) const {
			return costs_;
		}

		void setCosts(size_t costs) {
			costs_ = costs;
		}

		std::map<Player*, Field*>& getNextFields() {
			return nextField_;
		}

		void setNextDefaultField(Field* nextField) {
			nextDefaultField_ = nextField;
		}

		friend class GameContainer;
	private:
		Field();

		Field(const Field&);
		Field& operator=(const Field&);


		std::map<Player*, Field*> nextField_;
		size_t costs_;

		//NO OWNERSHIP
		Field* nextDefaultField_;
		
	};
}