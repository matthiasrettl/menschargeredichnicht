#pragma once

#include <iostream>
#include <sstream>
#include <string>

namespace UserInterface {

	class TextBox
	{
	public:
		TextBox(std::wstring header) :
			header_{ header },
			input_{}
		{};

		~TextBox() {};

		std::wstring *const ask() {
			printHeader();
			return readInput();
		}

	protected:
		void printHeader() const {
			std::wcout << header_ << std::endl;
		}

		std::wstring *const readInput() {
			std::getline(std::wcin, input_);
			return &input_;
		}

	private:
		std::wstring header_;
		std::wstring input_;
	};
}