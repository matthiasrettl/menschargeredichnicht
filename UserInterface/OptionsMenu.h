#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace UserInterface {

	class OptionsMenu
	{
	public:
		OptionsMenu(std::wstring header, std::vector<std::wstring> options, std::wstring inputRequest) :

			header_{header},
			options_{options},
			inputRequest_{inputRequest},
			input_{}
		{};

		~OptionsMenu() {};
	
		size_t ask() {
			printHeader();
			printOptions();
			
			do {
				printInputRequest();
				readInput();
			} while (input_ < 0 || input_ >= options_.size());

			return input_;
		}

		const std::wstring& getOptionText() const {
			return options_[input_];
		}

	protected:
		void printHeader() const {
			std::wcout << header_ << std::endl;
		}

		void printOptions() const {
			for (int optionId = 0; optionId < options_.size(); ++optionId)
				std::wcout << L" " << optionId << L": " + options_[optionId] << std::endl;
		}

		void printInputRequest() const {
			std::wcout << inputRequest_ << std::endl;
		}

		size_t readInput() {
			std::string line;
			std::getline(std::cin, line);

			try {
				input_ = std::stoi(line);
			} catch (const std::invalid_argument&) {
				// not an integer
				std::wcout << L"enter an integer value" << std::endl;
				input_ = -1;
			}
			
			return input_;
		}

	private:
		std::wstring header_;
		std::vector<std::wstring> options_;
		std::wstring inputRequest_;
		size_t input_;
	};
}