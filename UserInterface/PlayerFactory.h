#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include <functional>

#include "GameContainer.h"
#include "PlayerHuman.h"
#include "PlayerComputer.h"
#include "PlayerDummy.h"
#include "Rules.h"

#include "OptionsMenu.h"
#include "TextBox.h"

namespace UserInterface {

	class PlayerFactory
	{
	public:
		PlayerFactory(const std::unordered_map<wchar_t, std::wstring>* symbolsInUse,
			Game::GameContainer& gameContainer) :

			askPlayerType_{ L"player types", PLAYER_TYPES,
				L"enter player type id:"},
			askPlayerSymbol_{ L"enter a player symbol (e.g. ? or !,...):"},
			symbolsInUse_{*symbolsInUse },
			numHumanPlayers_{ 0 },
			numComputerPlayers_{ 0 },
			numDummyPlayers_{ 0 },
			gameContainer_{gameContainer}
		{};

		~PlayerFactory() {};

		void createPlayer() {
			const size_t playerId = gameContainer_.getPlayers()->size() + 1;

			// print header
			std::wcout << std::endl << std::endl
				<< "--- Player " << playerId << " ---" << std::endl;

			// select player type
			size_t playerTypeId = askPlayerType_.ask();
			const std::wstring& playerType = PLAYER_TYPES[playerTypeId];
			std::wcout << playerType << L" selected for player "
				<< playerId << L" as player type" << std::endl;

			if (playerType != PLAYER_TYPE_NO_PLAYER) {
				//select a player symbol
				std::wstring playerSymbolAnswer;
				wchar_t playerSymbol;

				// validate user input
				while (true) {
					playerSymbolAnswer = *askPlayerSymbol_.ask();
					
					if (playerSymbolAnswer.size() != 1) {
						std::wcout << L"enter a single character" << std::endl;
						continue;
					}
						
					playerSymbol = playerSymbolAnswer[0];
					if (symbolsInUse_.find(playerSymbol) !=
						symbolsInUse_.end()) {
						std::wcout << L"symbol " << playerSymbol 
							<< L" is already used by "
							<< symbolsInUse_[playerSymbol] << std::endl;

						continue;
					}
					
					break;
				}
				
				// mark user symbol as "symbolInUse" by player + playerid
				symbolsInUse_[playerSymbol] = L"Player " + playerId;
				std::wcout << playerSymbol << L" selected for player "
					<< playerId << L" as player symbol" << std::endl;

				// create player in game container
				if (playerType == PLAYER_TYPE_HUMAN) {
					Game::PlayerHuman* humanPlayer = 
						gameContainer_.addNewPlayer<Game::PlayerHuman>(
							playerSymbol);
					numHumanPlayers_++;

					humanPlayer->setUserChoiceFunction(
						Game::PlayerHuman::UserChoiceFunction{
							moveChoice});
				}
				else {
					gameContainer_.addNewPlayer<Game::PlayerComputer>(
						playerSymbol);
					numComputerPlayers_++;
				}
			}
			else {
				// this player is not active
				gameContainer_.addNewPlayer<Game::PlayerDummy>(L' ');
				numDummyPlayers_++;
			}
		}

		size_t countHumanPlayers() {
			return numHumanPlayers_;
		}
		size_t countComputerPlayers() {
			return numComputerPlayers_;
		}
		size_t countDummyPlayers() {
			return numDummyPlayers_;
		}

		static size_t moveChoice(std::vector<Game::Move> possibleMoves) {
			std::vector<std::wstring> moveDescriptions{};
			for (const Game::Move& move : possibleMoves)
				moveDescriptions.push_back(
					std::wstring{ move.second.begin(), move.second.end() });

			OptionsMenu moveChoice{ L"move options", 
				moveDescriptions, L"select a move option" };

			size_t choice = moveChoice.ask();
			return choice;
		}

		static const std::wstring PLAYER_TYPE_HUMAN;
		static const std::wstring PLAYER_TYPE_COMPUTER;
		static const std::wstring PLAYER_TYPE_NO_PLAYER;
		static const std::vector<std::wstring> PLAYER_TYPES;

	private:
		OptionsMenu askPlayerType_;
		TextBox askPlayerSymbol_;

		std::unordered_map<wchar_t, std::wstring> symbolsInUse_;
		size_t numHumanPlayers_;
		size_t numComputerPlayers_;
		size_t numDummyPlayers_;

		//NO OWNERSHIP
		Game::GameContainer& gameContainer_;
	};


	const std::wstring PlayerFactory::PLAYER_TYPE_HUMAN{
		L"human player" };
	const std::wstring PlayerFactory::PLAYER_TYPE_COMPUTER{
		L"computer player" };
	const std::wstring PlayerFactory::PLAYER_TYPE_NO_PLAYER{ 
		L"no player" };

	const std::vector<std::wstring> PlayerFactory::PLAYER_TYPES {
			PlayerFactory::PLAYER_TYPE_HUMAN, 
			PlayerFactory::PLAYER_TYPE_COMPUTER,
			PlayerFactory::PLAYER_TYPE_NO_PLAYER };
}