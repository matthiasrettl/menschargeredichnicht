#include "Console.h"


int main()
{
	// start window
	std::wcout << "----------------------------------------" << std::endl;
	std::wcout << "----------------------------------------" << std::endl;
	std::wcout << "---    Mensch aergere dich nicht     ---" << std::endl;
	std::wcout << "----------------------------------------" << std::endl;
	std::wcout << "----------------------------------------" << std::endl;
	std::wcout << std::endl;

	std::unordered_map<wchar_t, std::wstring> symbolsInUse{ 
		{L'0', L"stone identification"},
		{L'1', L"stone identification"},
		{L'2', L"stone identification"},
		{L'3', L"stone identification"},
		{L' ', L"board canvas"},
		{UserInterface::BoardCanvas::SYMBOL_BOARD_FIELD, L"board canvas"},
		{UserInterface::BoardCanvas::SYMBOL_BOARD_FIELD_DOWN, L"board canvas"} };

	//
	Game::GameContainer gameContainer{};

	// select player
	UserInterface::PlayerFactory playerFactory{ &symbolsInUse, gameContainer };
	playerFactory.createPlayer();
	playerFactory.createPlayer();
	playerFactory.createPlayer();
	playerFactory.createPlayer();
	
	if (playerFactory.countDummyPlayers() == 4) {
		std::wcout << L"No player selected at all" << std::endl;
		std::wcout << L"Press a key and enter to exit" << std::endl;

		int a;
		std::cin >> a;
		return 0;
	}

	// set up game
	gameContainer.createStonesAndFieldsFor4Players();
	Game::GameState* state = gameContainer.setGameStateToInitial();

	// every player can go out with one stone
	for (Game::Player* player : *gameContainer.getPlayers()) {
		if (player->dice() == 0) continue; //dummy player
		Game::Stone* stone = player->getStones()[0];
		Game::Field* start = stone->getHomeField()->next(*player);

		state->state_[stone] = start;
	}

	// draw board
	UserInterface::BoardCanvas board {gameContainer};
	board.assignFields();
	board.build(*state);
	board.draw();

	// game loop
	Game::Rules& rules = gameContainer.getRules();
	Game::Player* player = *gameContainer.getPlayers()->begin();
	Game::Player* winner = nullptr;
	while (!winner)
	{
		size_t dice = player->dice();

		if (dice != 0) {
			board.build(*gameContainer.getGameState(), player);
			board.draw();
			std::wcout << "player " << player->getPlayerId() + 1
				<< " plays the dice and gets a " << dice << std::endl;

			std::this_thread::sleep_for(std::chrono::milliseconds{ 100 });
		}

		gameContainer.setGameState(player->move(dice, *gameContainer.getGameState(), rules));

		player = gameContainer.nextPlayer(player, dice);
		winner = gameContainer.winner(*gameContainer.getGameState());
	}

	// game end
	board.build(*gameContainer.getGameState());
	board.draw();

	std::wcout << "----------------------------------------" << std::endl;
	std::wcout << "----------------------------------------" << std::endl;
	std::wcout << "-------    CONGRATULATIONS TO    -------" << std::endl;
	std::wcout << "-------         player " << winner->getPlayerId()+1 
									   << "         -------" << std::endl;
	std::wcout << "----------------------------------------" << std::endl;
	std::wcout << "----------------------------------------" << std::endl;

	// press enter to exit
	int a;
	std::cin >> a;
	return 0;
}
