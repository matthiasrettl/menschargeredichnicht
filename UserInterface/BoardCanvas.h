#pragma once

#include <iostream>
#include <sstream>
#include <array>
#include <algorithm>

#include "GameContainer.h"
#include "Field.h"
#include "Stone.h"
#include "Player.h"
#include "GameState.h"

namespace UserInterface {

	const size_t BOARD_COLS = 40;
	const size_t BOARD_ROWS = 9;
	const size_t PLAYER_DISTANCE = 10;

	template<class type>
	using BoardGrid = std::array<std::array<type, BOARD_COLS>, BOARD_ROWS>;

	class BoardCanvas
	{
	public:
		BoardCanvas(Game::GameContainer& gameContainer):
			boardSymbols_{},
			assignedFieds_{},
			gameContainer_{gameContainer}
		{};

		~BoardCanvas() {};

		void draw() const {
			std::wstringstream board{ std::iostream::out };
			board << std::endl << "________________________________________"
				<< std::endl << std::endl;
			
			for (int row = 0; row < BOARD_ROWS; ++row) {
				for (int col = 0; col < BOARD_COLS; ++col)
					board << boardSymbols_[row][col];

				board << std::endl;
			}

			board << "________________________________________" << std::endl;
			std::wcout << board.str() << std::flush;
		}

		void clean() {
			for (int row = 0; row < BOARD_ROWS; ++row)
				for (int col = 0; col < BOARD_COLS; ++col)
					boardSymbols_[row][col] = L' ';

			for (int row = 0; row < BOARD_ROWS/2; ++row)
				for (int col = 0; col < BOARD_COLS; col += PLAYER_DISTANCE)
					boardSymbols_[row][col] = SYMBOL_BOARD_FIELD_DOWN;

			for (int row = BOARD_ROWS / 2 + 1; row < BOARD_ROWS; ++row)
				for (int col = PLAYER_DISTANCE-1; col < BOARD_COLS; col += PLAYER_DISTANCE)
					boardSymbols_[row][col] = SYMBOL_BOARD_FIELD_DOWN;

			for (int col = 0; col < BOARD_COLS; ++col)
				boardSymbols_[BOARD_ROWS / 2][col] = SYMBOL_BOARD_FIELD;
		}

		void assignFields() {
			//assign home fields 
			for (size_t i = 0; i < 4; ++i) {
				Game::Player* player = gameContainer_.findPlayer(i);
				size_t col = i * PLAYER_DISTANCE;

				size_t row = 0;
				for (Game::Stone* stone : player->getStones()) {
					Game::Field* homeField = stone->getHomeField();
					
					assignedFieds_[row++][col] = homeField;
				}
			}

			//assign track fields
			Game::Player* firstPlayer = gameContainer_.findPlayer(0);
			Game::Field* trackField = firstPlayer->getStones()[0]->getHomeField()->next(*firstPlayer);
			for (int col = 0; col < BOARD_COLS; ++col) {
				assignedFieds_[BOARD_ROWS / 2][col] = trackField;

				if (trackField->getNextFields().size() > 0) {
					//assign goal fields
					Game::Field* goalField = trackField->getNextFields().begin()->second;
					for (int row = BOARD_ROWS / 2 + 1; row < BOARD_ROWS; ++row) {
						assignedFieds_[row][col] = goalField;
						goalField = goalField->next(*firstPlayer);
					}
				}

				trackField = trackField->next(*firstPlayer);
			}
		}

		void build(Game::GameState& gameState, Game::Player* currentPlayer = nullptr) {
			clean();

			auto& state = gameState.state_;
			for (int row = 0; row < BOARD_ROWS; ++row) {
				for (int col = 0; col < BOARD_COLS; ++col) {
					if (assignedFieds_[row][col]) {
						Game::Field* field = assignedFieds_[row][col];
						for (auto stateEntry : state) {
							if (stateEntry.second == field) {
								Game::Stone* stone = stateEntry.first;
								const Game::Player* player = stone->getPlayer();
								wchar_t playerSymbol = player->getPlayerSymbol();

								if (player == currentPlayer) {
									size_t stoneId = 0;
									for (; currentPlayer->getStones()[stoneId] != stone; stoneId++);
									
									playerSymbol = std::to_wstring(stoneId)[0];
								}

								if (playerSymbol != L' ') boardSymbols_[row][col] = playerSymbol;
							}
						}
					}
				}
			}
		}

		static const wchar_t SYMBOL_BOARD_FIELD = L'.';
		static const wchar_t SYMBOL_BOARD_FIELD_DOWN = L'.';
	protected:
	private:
		BoardGrid<wchar_t> boardSymbols_;
		BoardGrid<Game::Field*> assignedFieds_;

		//NO OWNERSHIP
		Game::GameContainer& gameContainer_;
	};
}